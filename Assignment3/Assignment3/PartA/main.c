#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <xmc_gpio.h>
#include <xmc_eru.h>
#include "KeyboardHID.h"
#include "german_keyboardCodes.h"

#define TICKS_PER_SECOND 1000

/* Macros: */
#define LED1 P1_1
#define LED2 P1_0

volatile uint32_t msTicks;
uint32_t currentTicksCount;
uint32_t previousTicksCount;

const char sendable[][3] = {
	['A'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_A},
	['B'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_B},
	['C'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_C},
	['D'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_D},
	['E'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_E},
	['F'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_F},
	['G'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_G},
	['H'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_H},
	['I'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_I},
	['J'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_J},
	['K'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_K},
	['L'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_L},
	['M'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_M},
	['N'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_N},
	['O'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_O},
	['P'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_P},
	['Q'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_Q},
	['R'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_R},
	['S'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_S},
	['T'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_T},
	['U'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_U},
	['V'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_V},
	['W'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_W},
	['X'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_X},
	['Y'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_Y},
	['Z'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0, GERMAN_KEYBOARD_SC_Z},
	['a'] = {0, 0, GERMAN_KEYBOARD_SC_A},
	['b'] = {0, 0, GERMAN_KEYBOARD_SC_B},
	['c'] = {0, 0, GERMAN_KEYBOARD_SC_C},
	['d'] = {0, 0, GERMAN_KEYBOARD_SC_D},
	['e'] = {0, 0, GERMAN_KEYBOARD_SC_E},
	['f'] = {0, 0, GERMAN_KEYBOARD_SC_F},
	['g'] = {0, 0, GERMAN_KEYBOARD_SC_G},
	['h'] = {0, 0, GERMAN_KEYBOARD_SC_H},
	['i'] = {0, 0, GERMAN_KEYBOARD_SC_I},
	['j'] = {0, 0, GERMAN_KEYBOARD_SC_J},
	['k'] = {0, 0, GERMAN_KEYBOARD_SC_K},
	['l'] = {0, 0, GERMAN_KEYBOARD_SC_L},
	['m'] = {0, 0, GERMAN_KEYBOARD_SC_M},
	['n'] = {0, 0, GERMAN_KEYBOARD_SC_N},
	['o'] = {0, 0, GERMAN_KEYBOARD_SC_O},
	['p'] = {0, 0, GERMAN_KEYBOARD_SC_P},
	['q'] = {0, 0, GERMAN_KEYBOARD_SC_Q},
	['r'] = {0, 0, GERMAN_KEYBOARD_SC_R},
	['s'] = {0, 0, GERMAN_KEYBOARD_SC_S},
	['t'] = {0, 0, GERMAN_KEYBOARD_SC_T},
	['u'] = {0, 0, GERMAN_KEYBOARD_SC_U},
	['v'] = {0, 0, GERMAN_KEYBOARD_SC_V},
	['w'] = {0, 0, GERMAN_KEYBOARD_SC_W},
	['x'] = {0, 0, GERMAN_KEYBOARD_SC_X},
	['y'] = {0, 0, GERMAN_KEYBOARD_SC_Y},
	['z'] = {0, 0, GERMAN_KEYBOARD_SC_Z},
	['1'] = {0, 0, GERMAN_KEYBOARD_SC_1_AND_EXCLAMATION},
	['2'] = {0, 0, GERMAN_KEYBOARD_SC_2_AND_QUOTES},
	['3'] = {0, 0, GERMAN_KEYBOARD_SC_3_AND_PARAGRAPH},
	['4'] = {0, 0, GERMAN_KEYBOARD_SC_4_AND_DOLLAR},
	['5'] = {0, 0, GERMAN_KEYBOARD_SC_5_AND_PERCENTAGE},
	['6'] = {0, 0, GERMAN_KEYBOARD_SC_6_AND_AMPERSAND},
	['7'] = {0, 0,
			 GERMAN_KEYBOARD_SC_7_AND_SLASH_AND_OPENING_BRACE},
	['8'] = {0, 0,
			 GERMAN_KEYBOARD_SC_8_AND_OPENING_PARENTHESIS_AND_OPENING_BRACKET},
	['9'] = {0, 0,
			 GERMAN_KEYBOARD_SC_9_AND_CLOSING_PARENTHESIS_AND_CLOSING_BRACKET},
	['0'] = {0, 0, GERMAN_KEYBOARD_SC_0_AND_EQUAL_AND_CLOSING_BRACE},
	['!'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_1_AND_EXCLAMATION},
	['@'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_Q},
	['#'] = {0, 0, GERMAN_KEYBOARD_SC_HASHMARK_AND_APOSTROPHE},
	['\''] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_HASHMARK_AND_APOSTROPHE},
	['$'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_4_AND_DOLLAR},
	['%'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_5_AND_PERCENTAGE},
	['&'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_6_AND_AMPERSAND},
	['('] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_8_AND_OPENING_PARENTHESIS_AND_OPENING_BRACKET},
	[')'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_9_AND_CLOSING_PARENTHESIS_AND_CLOSING_BRACKET},
	['='] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_0_AND_EQUAL_AND_CLOSING_BRACE},
	['\"'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			  GERMAN_KEYBOARD_SC_2_AND_QUOTES},
	['/'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_7_AND_SLASH_AND_OPENING_BRACE},
	['['] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_8_AND_OPENING_PARENTHESIS_AND_OPENING_BRACKET},
	[']'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_9_AND_CLOSING_PARENTHESIS_AND_CLOSING_BRACKET},
	['{'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_7_AND_SLASH_AND_OPENING_BRACE},
	['}'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_0_AND_EQUAL_AND_CLOSING_BRACE},
	['?'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_SHARP_S_AND_QUESTION_AND_BACKSLASH},
	['\\'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			  GERMAN_KEYBOARD_SC_SHARP_S_AND_QUESTION_AND_BACKSLASH},
	['^'] = {0, 0, GERMAN_KEYBOARD_SC_CARET_AND_DEGREE},
	['+'] = {0, 0, GERMAN_KEYBOARD_SC_PLUS_AND_ASTERISK_AND_TILDE},
	['*'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_PLUS_AND_ASTERISK_AND_TILDE},
	['~'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_PLUS_AND_ASTERISK_AND_TILDE},
	['-'] = {0, 0, GERMAN_KEYBOARD_SC_MINUS_AND_UNDERSCORE},
	['_'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_MINUS_AND_UNDERSCORE},
	[','] = {0, 0, GERMAN_KEYBOARD_SC_COMMA_AND_SEMICOLON},
	[';'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_COMMA_AND_SEMICOLON},
	['.'] = {0, 0, GERMAN_KEYBOARD_SC_DOT_AND_COLON},
	[':'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_DOT_AND_COLON},
	['<'] = {0, 0, GERMAN_KEYBOARD_SC_LESS_THAN_AND_GREATER_THAN_AND_PIPE},
	['>'] = {HID_KEYBOARD_MODIFIER_LEFTSHIFT, 0,
			 GERMAN_KEYBOARD_SC_LESS_THAN_AND_GREATER_THAN_AND_PIPE},
	['|'] = {HID_KEYBOARD_MODIFIER_RIGHTALT, 0,
			 GERMAN_KEYBOARD_SC_LESS_THAN_AND_GREATER_THAN_AND_PIPE},
	//	['\0']= {0, 0, GERMAN_KEYBOARD_SC_ENTER}, Avoid enter upon end of string
	['\n'] = {0, 0, GERMAN_KEYBOARD_SC_ENTER},
	[' '] = {0, 0, GERMAN_KEYBOARD_SC_SPACE}};

static char pwChars[85] = "aabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!()-_+=~;:,.<>[]{}/?&$";

static bool initDone = false;
static bool nmLkInitDone = false;
static bool cpsLkInitDone = false;
static bool loggedIn = false;
static bool waitOnceAfterLogin = false;
static uint32_t a = 0;
static uint8_t pwIndex = 0;
bool send = false;
static char pwd[100] = {'\0'};
static char correctChar;
static uint32_t procTime[86];
static bool print = false;
static char stringToSend[61] = {'\0'};
static uint8_t characterSent = 0;
static uint8_t indexToSend = 0;
static uint8_t lengthToSend = 0;
static char timeStr[100] = {'\0'};
static int lrLmt;
static int urLmt;
static char createTxtFileCmd[61] = {"ECHO \"your name\" >> ~/1234567\n"};
static bool waitOnceToInit = true;

/* Clock configuration */
XMC_SCU_CLOCK_CONFIG_t clock_config = {
	.syspll_config.p_div = 2,
	.syspll_config.n_div = 80,
	.syspll_config.k_div = 4,
	.syspll_config.mode = XMC_SCU_CLOCK_SYSPLL_MODE_NORMAL,
	.syspll_config.clksrc = XMC_SCU_CLOCK_SYSPLLCLKSRC_OSCHP,
	.enable_oschp = true,
	.calibration_mode = XMC_SCU_CLOCK_FOFI_CALIBRATION_MODE_FACTORY,
	.fsys_clksrc = XMC_SCU_CLOCK_SYSCLKSRC_PLL,
	.fsys_clkdiv = 1,
	.fcpu_clkdiv = 1,
	.fccu_clkdiv = 1,
	.fperipheral_clkdiv = 1};

void SysTick_Handler(void)
{
	msTicks++;
}

static void delay_ms(uint32_t dlyTicks)
{
	uint32_t curTicks;

	curTicks = msTicks;
	while ((msTicks - curTicks) < dlyTicks)
	{
		__NOP();
	}
}

/* Forward declaration of HID callbacks as defined by LUFA */
bool CALLBACK_HID_Device_CreateHIDReport(
	USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
	uint8_t *const ReportID,
	const uint8_t ReportType,
	void *ReportData,
	uint16_t *const ReportSize);

void CALLBACK_HID_Device_ProcessHIDReport(
	USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
	const uint8_t ReportID,
	const uint8_t ReportType,
	const void *ReportData,
	const uint16_t ReportSize);

void SystemCoreClockSetup(void);

/**
 * Main program entry point. This routine configures the hardware required by
 * the application, then enters a loop to run the application tasks in sequence.
 */

int main(void)
{
	// Init LED pins for debugging and NUM/CAPS visual report
	XMC_GPIO_SetMode(LED1, XMC_GPIO_MODE_OUTPUT_PUSH_PULL);
	XMC_GPIO_SetMode(LED2, XMC_GPIO_MODE_OUTPUT_PUSH_PULL);
	USB_Init();
	SysTick_Config(SystemCoreClock / TICKS_PER_SECOND);
	// Wait until host has enumerated HID device
	for (int i = 0; i < 10e6; ++i)
		;

	while (1)
	{
		HID_Device_USBTask(&Keyboard_HID_Interface);
		// if (!initDone)
		// {
		// 	if (nmLkInitDone && cpsLkInitDone)
		// 	{
		// 		initDone = true;
		// 		loggedIn = false;
		// 		// delay_ms(2000);
		// 	}
		// }
		if (waitOnceToInit)
		{
			delay_ms(1000);
			waitOnceToInit = false;
			initDone = true;
			loggedIn = false;
		}
		if (initDone && loggedIn && (!waitOnceAfterLogin))
		{
			delay_ms(300);
		}
	}
}

// Callback function called when a new HID report needs to be created
bool CALLBACK_HID_Device_CreateHIDReport(
	USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
	uint8_t *const ReportID,
	const uint8_t ReportType,
	void *ReportData,
	uint16_t *const ReportSize)
{
	USB_KeyboardReport_Data_t *report = (USB_KeyboardReport_Data_t *)ReportData;
	*ReportSize = sizeof(USB_KeyboardReport_Data_t);

	// string to be sent

	if ((initDone && (send || (indexToSend < lengthToSend) /*|| print */|| loggedIn)))
	{
		if ((pwIndex < 85) || (indexToSend < lengthToSend) || loggedIn)
		{
			if (indexToSend < lengthToSend)
			{
				if (characterSent)
				{
					report->Modifier = 0;
					report->Reserved = 0;
					report->KeyCode[0] = 0;
					characterSent = 0;
					indexToSend++;
				}
				else
				{
					report->Modifier = sendable[(uint8_t)stringToSend[indexToSend]][0];
					report->Reserved = sendable[(uint8_t)stringToSend[indexToSend]][1];
					report->KeyCode[0] = sendable[(uint8_t)stringToSend[indexToSend]][2];
					characterSent = 1;
				}
			}
			else
			{
				strcpy(stringToSend, "");
				/*if (print)
				{
					strcat(stringToSend, (char[2]){(char)pwChars[pwIndex], '-'});
					sprintf(timeStr, "%lu", procTime[pwIndex]);
					strcat(strcat(stringToSend, timeStr), " | ");
				}
				else*/ if (loggedIn)
				{
					strcpy(stringToSend, createTxtFileCmd);
					loggedIn = false;
					characterSent = 0;
				}
				else
				{
					strcat(stringToSend, pwd);
					strcat(strcat(stringToSend, (char[2]){(char)pwChars[pwIndex], '\0'}), "\n");
				}

				pwIndex++;

				lengthToSend = strlen(stringToSend);
				indexToSend = 0;
				send = false;
			}
		}
		else
		{

			int highestProcTimeIdx = 1;
			for (int i = 1; i < 85; i++)
			{
				if (procTime[i] > procTime[highestProcTimeIdx])
				{
					highestProcTimeIdx = i;
				}
			}
			correctChar = pwChars[highestProcTimeIdx];

			strcat(pwd, (char[2]){(char)correctChar, '\0'});
			// if (print)
			// {
			// 	print = false;
			// }
			// if (strlen(pwd) == 5)
			// {
			// 	print = true;
			// }

			strcpy(stringToSend, "");
			pwIndex = 0;
			lengthToSend = 0;
			indexToSend = 0;
			send = true;
		}
	}
	return true;
}

// Called on report input. For keyboard HID devices, that's the state of the LEDs
void CALLBACK_HID_Device_ProcessHIDReport(
	USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo,
	const uint8_t ReportID,
	const uint8_t ReportType,
	const void *ReportData,
	const uint16_t ReportSize)
{
	uint8_t *report = (uint8_t *)ReportData;

	if (*report & HID_KEYBOARD_LED_NUMLOCK)
	{

		procTime[pwIndex - 1] = msTicks - a;
		nmLkInitDone = true;
		send = true;
		XMC_GPIO_SetOutputHigh(LED1);
	}
	else
	{
		send = false;
		a = msTicks;
		XMC_GPIO_SetOutputLow(LED1);
	}
	if (*report & HID_KEYBOARD_LED_CAPSLOCK)
	{
		XMC_GPIO_SetOutputHigh(LED2);
		loggedIn = true;
		cpsLkInitDone = true;
	}
	else
	{
		XMC_GPIO_SetOutputLow(LED2);
		loggedIn = false;
	}
}

void SystemCoreClockSetup(void)
{
	/* Setup settings for USB clock */
	XMC_SCU_CLOCK_Init(&clock_config);

	XMC_SCU_CLOCK_EnableUsbPll();
	XMC_SCU_CLOCK_StartUsbPll(2, 64);
	XMC_SCU_CLOCK_SetUsbClockDivider(4);
	XMC_SCU_CLOCK_SetUsbClockSource(XMC_SCU_CLOCK_USBCLKSRC_USBPLL);
	XMC_SCU_CLOCK_EnableClock(XMC_SCU_CLOCK_USB);

	SystemCoreClockUpdate();
}