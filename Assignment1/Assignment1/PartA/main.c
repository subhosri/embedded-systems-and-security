#include <xmc_gpio.h>
#include <xmc_eru.h>

#define TICKS_PER_SECOND 1000

/* GPIO configuration */

#define GPIO_LED1 XMC_GPIO_PORT1,  1

volatile uint32_t msTicks;

void SysTick_Handler(void)
{
  msTicks++;
}

static void delay_ms(uint32_t dlyTicks)
{
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks)
  {
    __NOP();
  }
}

static void initLed2_P1_1(void)
{

  const XMC_GPIO_CONFIG_t LED_config =
      {.mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
       .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW,
       .output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE};

  XMC_GPIO_Init(GPIO_LED1, &LED_config);
}

static void generateDelayBetweenSignals()
{
  delay_ms(100);
}

static void generateDelayBetweenLetters()
{
  delay_ms(300);
}

static void generateDelayBetweenWords()
{
  delay_ms(700);
}

static void turnOnLed2_ms(uint32_t dlyTicks)
{
  XMC_GPIO_SetOutputHigh(GPIO_LED1);
  delay_ms(dlyTicks);
  XMC_GPIO_SetOutputLow(GPIO_LED1);
}

static void transmitDot()
{
  turnOnLed2_ms(100);
}

static void transmitDash()
{
  turnOnLed2_ms(300);
}


int main(void)
{

  SysTick_Config(SystemCoreClock / TICKS_PER_SECOND);

  initLed2_P1_1();

  char codedMessage[] = ".s.w-s.s-s.l.s-l-s.w-s-l-s-s-l.s-s.l.s.s.l.";
  int lengthOfMessage = (int)(sizeof(codedMessage) / sizeof(codedMessage[0]));

  while (1)
  {

    for (int index = 0; index < lengthOfMessage; index++)
    {
      switch (codedMessage[index])
      {
      case '.':
        transmitDot();
        break;
      case '-':
        transmitDash();
        break;
      case 's':
        generateDelayBetweenSignals();
        break;
      case 'l':
        generateDelayBetweenLetters();
        break;
      case 'w':
        generateDelayBetweenWords();
        break;
      default:
        break;
      }
    }
    delay_ms(5000);
  }

  return 0;
}
