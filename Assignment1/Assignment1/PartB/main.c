#include <xmc_gpio.h>
#include <xmc_eru.h>
#include <stdio.h>
#include <string.h>

#define TICKS_PER_SECOND 1000

/* GPIO configuration */
#define GPIO_LED1 XMC_GPIO_PORT1, 1
#define GPIO_BUTTON1 XMC_GPIO_PORT1, 14
#define GPIO_BUTTON2 XMC_GPIO_PORT1, 15

volatile uint32_t msTicks;
uint32_t currentTicksCount;
uint32_t previousTicksCount;

void SysTick_Handler(void)
{
  msTicks++;
}

static void hw_delay_ms(uint32_t dlyTicks)
{
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks)
  {
    __NOP();
  }
}

static void initLed2_P1_1(void)
{

  const XMC_GPIO_CONFIG_t LED_config =
      {.mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
       .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW,
       .output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE};

  XMC_GPIO_Init(GPIO_LED1, &LED_config);
}

static void initButton1_P1_14_15(void)
{
  const XMC_GPIO_CONFIG_t in_config =
      {.mode = XMC_GPIO_MODE_INPUT_TRISTATE,
       .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW,
       .output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE};
  XMC_GPIO_Init(GPIO_BUTTON1, &in_config);
  XMC_GPIO_Init(GPIO_BUTTON2, &in_config);
}

static void turnOnLed_us_by_hw_delay(uint32_t dlyTicks)
{
  XMC_GPIO_SetOutputHigh(GPIO_LED1);
  hw_delay_ms(dlyTicks);
  XMC_GPIO_SetOutputLow(GPIO_LED1);
}

static void transmitDotByHWDelay()
{
  turnOnLed_us_by_hw_delay(100);
}

static void transmitDashByHWDelay()
{
  turnOnLed_us_by_hw_delay(300);
}

static void generateHWDelayBetweenSignals()
{
  hw_delay_ms(100);
}

static void generateHWDelayBetweenLetters()
{
  hw_delay_ms(300);
}

static void generateHWDelayBetweenWords()
{
  hw_delay_ms(700);
}

void transmitMessageByTypeByHWDelay(char typeTosend)
{
  switch (typeTosend)
  {
  case '.':
    transmitDotByHWDelay();
    break;
  case '-':
    transmitDashByHWDelay();
    break;
  case 's':
    generateHWDelayBetweenSignals();
    break;
  case 'l':
    generateHWDelayBetweenLetters();
    break;
  case 'w':
    generateHWDelayBetweenWords();
    break;
  default:
    break;
  }
}

int main(void)
{

  SysTick_Config(SystemCoreClock / TICKS_PER_SECOND);

  initLed2_P1_1();
  initButton1_P1_14_15();

  previousTicksCount = 0;
  currentTicksCount = 0;

  char codedMessage[] = ".s.w-s.s-s.l.s-l-s.w-s-l-s-s-l.s-s.l.s.s.l.";
  int lengthOfMessage = strlen(codedMessage);

  while (1)
  {
    // if button 1 press, then send string
    if (XMC_GPIO_GetInput(GPIO_BUTTON1) == 0)
    {
      previousTicksCount = currentTicksCount;
      currentTicksCount = msTicks;

      for (int index = 0; index < lengthOfMessage; index++)
      {
        transmitMessageByTypeByHWDelay(codedMessage[index]);
      }
    }
    // if button 2 press, then send string
    if (XMC_GPIO_GetInput(GPIO_BUTTON2) == 0)
    {

      uint32_t numberToTransmit = currentTicksCount - previousTicksCount;
      char codedDigit[1024] = {0};
      char numberToTransmitArray[16];
      int lengthOfNumberArray = sprintf(numberToTransmitArray, "%ld", numberToTransmit);

      for (int i = 0; i < lengthOfNumberArray; i++)
      {
        switch (numberToTransmitArray[i])
        {
        case '1':
          strcat(codedDigit, ".s-s-s-s-");
          break;
        case '2':
          strcat(codedDigit, ".s.s-s-s-");
          break;
        case '3':
          strcat(codedDigit, ".s.s.s-s-");
          break;
        case '4':
          strcat(codedDigit, ".s.s.s.s-");
          break;
        case '5':
          strcat(codedDigit, ".s.s.s.s.");
          break;
        case '6':
          strcat(codedDigit, "-s.s.s.s.");
          break;
        case '7':
          strcat(codedDigit, "-s-s.s.s.");
          break;
        case '8':
          strcat(codedDigit, "-s-s-s.s.");
          break;
        case '9':
          strcat(codedDigit, "-s-s-s-s.");
          break;
        case '0':
          strcat(codedDigit, "-s-s-s-s-");
          break;
        default:
          break;
        }
        if (i != (lengthOfNumberArray - 1))
          strcat(codedDigit, "l");
      }

      int lengthOfCodedDigit = strlen(codedDigit);
      for (int index = 0; index < lengthOfCodedDigit; index++)
      {
        transmitMessageByTypeByHWDelay(codedDigit[index]);
      }
    }
  }

  return 0;
}
